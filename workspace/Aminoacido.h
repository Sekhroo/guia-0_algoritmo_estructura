#ifndef AMINOACIDO_H
#define AMINOACIDO_H

#include <list>

#include "Atomo.h"

class Aminoacido {
    private:
        string nombre = "\0";
        int numero = 0;
        list<Atomo> list_atom;
        Atomo at;
        
    public:
        /* constructores */
        Aminoacido ();
        Aminoacido (string nombre, int numero);
        
        /* métodos get and set */
        string get_nombre();
        int get_numero();
        list<Atomo> get_list_atomos();

        void set_nombre(string nombre);
        void set_numero(int numero);
        void add_list_atom(Atomo atom);

        string menu_atomos(string option_atomos);
        void generar_atomos();
        void print_atom();
        void clear();
};
#endif