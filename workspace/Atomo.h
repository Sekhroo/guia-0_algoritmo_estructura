#ifndef ATOMO_H
#define ATOMO_H

#include <list>

#include "Coordenada.h"

class Atomo {
    private:
        string nombre = "\0";
        int numero = 0;
        Coordenada coord;
        
    public:
        /* constructores */
        Atomo ();
        Atomo (string nombre, int numero);
        
        /* métodos get and set */
        string get_nombre();
        int get_numero();
        Coordenada get_coordenada();

        void set_nombre(string nombre);
        void set_numero(int numero);
        void set_coordenada(Coordenada coord);
};
#endif