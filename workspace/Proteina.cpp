#include <list>
#include <iostream>
using namespace std;

#include "Proteina.h"
#include "Cadena.h"

/* constructores */
Proteina::Proteina() {
    string nombre = "\0";
    string id = "\0";
    list<Cadena> list_chain;
    Cadena chain;
}

Proteina::Proteina (string nombre, string id) {
    this-> nombre = nombre;
    this-> id = id;
}

/* métodos get and set */
string Proteina::get_nombre() {
    return this->nombre;
}

string Proteina::get_id() {
    return this->id;
}

list<Cadena> Proteina::get_list_chain() {
    return this->list_chain;
}
        

void Proteina::set_nombre(string nombre) {
    this->nombre = nombre;
}

void Proteina::set_id(string id) {
    this->id = id;
}

void Proteina::add_list_cadena(Cadena chain) {
    this->list_chain.push_back(chain);
}

//metodos
//menu de las cadenas: indica si desea añadir cadenas
string Proteina::menu_cadena(string option_cadena){

    cout << "======== Menu ========" << endl;
    cout << "Añadir cadena ____ [1]" << endl;
    cout << "No añadir cadena _ [0]" << endl << endl;
    cout << "======================" << endl;
    cout << "Opción: ";

    cin >> option_cadena;
    cout << endl;

    return option_cadena;
}

//Siguiente metodo de las funciones anidadas para generar objetos y almacenarlos en lista
void Proteina::generar_cadena(){
    string option_cadena = "\0";
    string name_chain = "\0";


    while(option_cadena != "0"){
        option_cadena = menu_cadena(option_cadena);

        if(option_cadena == "1"){
            clear();

            cout << "[string] Nombre de la cadena: ";
            cin >> name_chain;

            chain = Cadena(name_chain);

            //siguiente parte de la funcion anidada
            chain.generar_aminoacidos();

            add_list_cadena(chain);
            cout << endl;
            
        }else{
            option_cadena = "0";
            clear();
        }
    }
}

//Funcion anidada para imprimir la lista de proteinas y sus derivaciones (atributos y objetos enlazados)
void Proteina::print_cadena(){
    for(Cadena chain:get_list_chain()) {
        cout << "\tCadena: " << chain.get_letra() << endl;

        chain.print_aa();
    }
}

//Funcion para limpiar
void Proteina::clear() {
    cout << "\x1B[2J\x1B[H";
}