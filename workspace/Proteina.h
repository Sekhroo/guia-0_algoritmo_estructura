#ifndef PROTEINA_H
#define PROTEINA_H

#include <list>

#include "Cadena.h"

class Proteina {
    private:
        string nombre = "\0";
        string id = "\0";
        list<Cadena> list_chain;
        Cadena chain;
        
    public:
        /* constructores */
        Proteina ();
        Proteina (string nombre, string id);
        
        /* métodos get and set */
        string get_nombre();
        string get_id();
        list<Cadena> get_list_chain();

        void set_nombre(string nombre);
        void set_id(string id);
        void add_list_cadena(Cadena chain);

        string menu_cadena(string option_cadena);
        void generar_cadena();
        void print_cadena();
        void clear();
};
#endif