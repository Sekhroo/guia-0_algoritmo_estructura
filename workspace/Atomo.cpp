#include <list>
#include <iostream>
using namespace std;

#include "Atomo.h"
#include "Coordenada.h"

/* constructores */
Atomo::Atomo() {
    string nombre = "\0";
    int numero = 0;
    Coordenada coord;
}

Atomo::Atomo (string nombre, int numero) {
    this-> nombre = nombre;
    this-> numero = numero;
}

/* métodos get and set */
string Atomo::get_nombre() {
    return this-> nombre;
}

int Atomo::get_numero() {
    return this-> numero;
}

Coordenada Atomo::get_coordenada() {
    return this-> coord;
}


void Atomo::set_nombre(string nombre) {
    this-> nombre = nombre;
}
        
void Atomo::set_numero(int numero) {
    this-> numero = numero;
}

void Atomo::set_coordenada(Coordenada coord) {
    this-> coord = coord;
}