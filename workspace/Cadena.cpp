#include <list>
#include <iostream>
using namespace std;

#include "Cadena.h"
#include "Aminoacido.h"

/* constructores */
Cadena::Cadena() {
    string letra = "\0";
    list<Aminoacido> list_aa;
    Aminoacido aa;
}

Cadena::Cadena (string letra) {
    this-> letra = letra;
}

/* métodos get and set */
string Cadena::get_letra() {
    return this-> letra;
}

list<Aminoacido> Cadena::get_list_aa(){
    return this-> list_aa;
}


void Cadena::set_letra(string letra) {
    this->letra = letra;
}

void Cadena::add_list_aminoacido(Aminoacido aa) {
    this->list_aa.push_back(aa);
}

//metodos
//menu de aminoacidos: indicar si el usuario desea seguir añadiendo elementos
string Cadena::menu_aminoacidos(string option_aminoacidos){

    cout << "========== Menu ==========" << endl;
    cout << "Añadir aminoacido ____ [1]" << endl;
    cout << "No añadir aminoacido _ [0]" << endl << endl;
    cout << "==========================" << endl;
    cout << "Opción: ";

    cin >> option_aminoacidos;
    cout << endl;

    return option_aminoacidos;
}

//Siguiente parte de la funcion anidada para generar objetos y almacenarlos en listas
void Cadena::generar_aminoacidos(){
    string option_aminoacidos = "\0";
    string letra = "\0";
    int numero = 0;

    while(option_aminoacidos != "0"){
        option_aminoacidos = menu_aminoacidos(option_aminoacidos);

        if(option_aminoacidos == "1"){
            clear();

            cout << "[string] Letra del aminoacido: ";
            cin >> letra;
            cout << endl;
            cout << "[int] Cantidad de aminoacido: ";
            cin >> numero;

            aa = Aminoacido(letra,numero);

            //Siguiente metodo de las funciones anidadas para generar objetos y almacenarlos en lista
            aa.generar_atomos();

            add_list_aminoacido(aa);
            cout << endl;
            
        }else{
            option_aminoacidos = "0";
            clear();
        }
    }
}

//Funcion anidada: imprimir todos los elementos de las listas mediante for
void Cadena::print_aa(){
    for(Aminoacido aa: get_list_aa()) {
        cout << "\t\tAminoacido: " << aa.get_nombre() << ": " << aa.get_numero() << endl;

        aa.print_atom();
    }
}

//funcion para limpiar la terminal
void Cadena::clear() {
    cout << "\x1B[2J\x1B[H";
}