#include <list>
#include <iostream>
using namespace std;

#include "Aminoacido.h"
#include "Atomo.h"

/* constructores */
Aminoacido::Aminoacido() {
    string nombre = "\0";
    int numero = 0;
    list<Atomo> list_atom;
    Atomo at;
}

Aminoacido::Aminoacido (string nombre, int numero) { 
    this->nombre = nombre;
    this->numero = numero;
}

/* métodos get and set */
string Aminoacido::get_nombre() {
    return this-> nombre;
}

int Aminoacido::get_numero() {
    return this-> numero;
}

list<Atomo> Aminoacido::get_list_atomos(){
    return this-> list_atom;
}


void Aminoacido::set_nombre(string nombre) {
    this->nombre = nombre;
}
        
void Aminoacido::set_numero(int numero) {
    this->numero = numero;
}

void Aminoacido::add_list_atom(Atomo atom) {
    this->list_atom.push_back(atom);
}

//Metodos
//Ultimo menu para preguntar al usuario si desea  seguir añadiendo elementos
string Aminoacido::menu_atomos(string option_atomos){

    cout << "======== Menu ========" << endl;
    cout << "Añadir átomo ____ [1]" << endl;
    cout << "No añadir átomo _ [0]" << endl << endl;
    cout << "======================" << endl;
    cout << "Opción: ";

    cin >> option_atomos;
    cout << endl;

    return option_atomos;
}

//Final de las funciones anidadas para generar objetos y almacenarlos en listas
void Aminoacido::generar_atomos(){
    string option_atomos = "\0";
    string nombre = "\0";
    int numero = 0;
    float x_atom = 0.0;
    float y_atom = 0.0;
    float z_atom = 0.0;


    while(option_atomos != "0"){
        option_atomos = menu_atomos(option_atomos);

        if(option_atomos == "1"){
            clear();

            cout << "[string] Nombre del átomo: ";
            cin >> nombre;
            cout << endl;
            cout << "[int] Cantidad de átomos: ";
            cin >> numero;
            cout << endl;

            at = Atomo(nombre,numero);

            cout << "[float] Coordenada x del átomo: ";
            cin >> x_atom;
            at.get_coordenada().set_x(x_atom);
            cout << endl;
            cout << "[float] Coordenada y del átomo: ";
            cin >> y_atom;
            at.get_coordenada().set_y(y_atom);
            cout << endl;
            cout << "[float] Coordenada z del átomo: ";
            cin >> z_atom;
            at.get_coordenada().set_z(z_atom);
            
            clear();

            add_list_atom(at);
            cout << endl;
            
        }else{
            option_atomos = "0";
            clear();
        }
    }
}

//Funcion final de las anidadas para imprimir todos los elementos almacenados en listas
void Aminoacido::print_atom(){
    for(Atomo at: get_list_atomos()){
        cout << "\t\t\tAtomo: " << at.get_nombre() << ": " << at.get_numero() << "[" << 
        at.get_coordenada().get_x() << ", " << at.get_coordenada().get_y() << ", " <<
        at.get_coordenada().get_z() << "]" << endl;
    }
}

//funcion para limpiar
void Aminoacido::clear() {
    cout << "\x1B[2J\x1B[H";
}