#include <list>
#include <iostream>
using namespace std;

#include "Proteina.h"
#include "Proteina.cpp"

#include "Cadena.h"
#include "Cadena.cpp"

#include "Aminoacido.h"
#include "Aminoacido.cpp"

#include "Atomo.h"
#include "Atomo.cpp"

#include "Coordenada.h"
#include "Coordenada.cpp"

/* Función para limpiar la terminal */
void clear() {
    cout << "\x1B[2J\x1B[H";
}

/* Función menu: metodos principales */
string menu_principal (string opt) {
    cout << "========= Menu =========" << endl;
    cout << "Añadir proteina ___ [1]" << endl;
    cout << "Mostrar proteinas _ [2]" << endl << endl;
    cout << "Cerrar programa ___ [0]" << endl;
    cout << "========================" << endl;
    cout << "Opción: ";

    cin >> opt;
    
    clear();

    return opt;
}

/* Función anidada para imprimir los objetos almacenados en listas con un for */
void imprimir_datos_proteinas(list<Proteina> proteinas, Proteina p) {
    for(Proteina p: proteinas) {
        cout << "=================================================================" << endl;
        cout << "Nombre: " << p.get_nombre() << endl <<"ID: " << p.get_id() << endl;
        
        p.print_cadena();
    }
}

int main() {
    //Instanciar variables y lista
    list <Proteina> proteinas;
    string option = "\0";
    
    //Añadir proteina base --- instancia una proteina
    Proteina p = Proteina("Ebola","6EHM");
    
    //la agrega a la lista
    proteinas.push_back(p);

    //INICIO DEL MENÚ
    while (option != "0") {
        //llamada al menu principal
        option = menu_principal(option);

        /* Añadir proteina */
        if (option == "1") {
            string name_protein = "\0";
            string id_protein = "\0";
            
            //pregunta al usuario
            cout << "[string] Nombre de la proteina: ";
            cin >> name_protein;
            cout << endl;
            cout << "[string] ID de la proteina: ";
            cin >> id_protein;

            //nueva proteina
            p = Proteina(name_protein,id_protein);
            
            cout << endl;

            //Inicio del metodo anidado para añadir nuevos objetos(cadena, aminoacidos, atomo)
            p.generar_cadena();

            proteinas.push_back(p);
        }

        /* Mostrar proteinas */
        if (option == "2") {
            imprimir_datos_proteinas(proteinas, p);
        }
    }

    return 0;
}