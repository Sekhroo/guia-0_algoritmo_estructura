#ifndef CADENA_H
#define CADENA_H

#include <list>

#include "Aminoacido.h"

class Cadena {
    private:
        string letra = "\0";
        list<Aminoacido> list_aa;
        Aminoacido aa;

    public:
        /* constructores */
        Cadena ();
        Cadena (string letra);
        
        /* métodos get and set */
        string get_letra();
        list<Aminoacido> get_list_aa();

        void set_letra(string letra);
        void add_list_aminoacido(Aminoacido aa);

        string menu_aminoacidos(string option_aminoacidos);
        void generar_aminoacidos();
        void print_aa();
        void clear();
};
#endif